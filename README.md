# Population Mean

Example of R function with tests.

## Requirements

- R version `4.0.3`
- [testthat](https://testthat.r-lib.org/) package `3.0.1`

## How to Run Tests

In console navigate to the directory where this README.md file is located and run

```
./run_tests.sh
```

**Expected output**

```
✔ |  OK F W S | Context
✔ |   5       | popmean [0.3 s]                                                 

══ Results ══
Duration: 0.3 s

[ FAIL 0 | WARN 0 | SKIP 0 | PASS 5 ]
```

![](src/run_tests.png)